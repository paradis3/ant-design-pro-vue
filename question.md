# Some questions

1. 为什么我的 console 没有效果？
  1.1 实在是让人费解
2. 不想用目前的网络数据响应格式？
  2.1 试下修改拦截器
3. 好多语法看不懂啊...

:::

问题1 或许就出在这里

```javascript
const IS_PROD = ['production', 'prod'].includes(process.env.NODE_ENV)

const plugins = []
if (IS_PROD) {
  plugins.push('transform-remove-console')
}
```

## 解决

改了下环境的配置，重启项目，问题1 fixed；
问题2 貌似也不怎么需要改动

得花时间复习下ES6语法了
